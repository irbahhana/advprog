package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.NoCrustSandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThickBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThinBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.*;

public class Myburger {
    public static void main(String[] args) {
        Food food = new NoCrustSandwich();

        Food food1 = new ThickBunBurger();
        food1 = new Cheese(food1);
        food1 = new ChickenMeat(food1);
        food1 = new Cucumber(food1);

        Food food2 = new ThinBunBurger();
        food2 = new ChickenMeat(food2);
        food2 = new BeefMeat(food2);
        food2 = new Tomato(food2);

        System.out.println("Thanks for ordering");
        System.out.println("bill");
        System.out.println("--------------------");
        System.out.println(food.getDescription() + " $" +food.cost());
        System.out.println(food1.getDescription() + " $" +food1.cost());
        System.out.println(food2.getDescription() + " $" +food2.cost());
    }
}
