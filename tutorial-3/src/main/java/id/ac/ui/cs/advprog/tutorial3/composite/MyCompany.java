package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.*;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

public class MyCompany {
    public static void main(String[] args) {
        Company mycompany = new Company();
        FrontendProgrammer naufal = new FrontendProgrammer("Romario",2000000);
        SecurityExpert yobshet = new SecurityExpert("Ricardo",100000.0);
        NetworkExpert ayah = new NetworkExpert("yaboi",259923);
        mycompany.addEmployee(ayah);
        mycompany.addEmployee(naufal);
        mycompany.addEmployee(yobshet);

        System.out.println("Net Salaries : " + mycompany.getNetSalaries());
    }
}
