package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChiliSauce extends Food{
    Food chili;

    public ChiliSauce(Food chili){
        this.chili = chili;
    }

    @Override
    public String getDescription() {
        return chili.getDescription() + ", adding chili sauce";
    }

    @Override
    public double cost() {
        return chili.cost() + 0.3;
    }
}
