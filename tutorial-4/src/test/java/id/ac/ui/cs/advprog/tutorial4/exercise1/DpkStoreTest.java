package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

import org.junit.Before;
import org.junit.Test;


public class DpkStoreTest {
    protected DepokPizzaStore depok;
    protected Pizza pizza;
    protected String expected;

    @Before
    public void setUp() {
        depok = new DepokPizzaStore();
    }

    @Test
    public void testCreatePizzaCheese() {
        pizza = depok.createPizza("cheese");
        expected = "Depok style cheese pizza";
        assertEquals(expected, pizza.getName());
    }

    @Test
    public void testCreatePizzaVeggie() {
        pizza = depok.createPizza("veggie");
        expected = "Depok style veggie pizza";
        assertEquals(expected, pizza.getName());
    }

    @Test
    public void testCreatePizzaClam() {
        pizza = depok.createPizza("clam");
        expected = "Depok style clam pizza";
        assertEquals(expected, pizza.getName());
    }

}
