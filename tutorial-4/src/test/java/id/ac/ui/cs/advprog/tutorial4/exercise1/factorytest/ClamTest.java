package id.ac.ui.cs.advprog.tutorial4.exercise1.factorytest;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.*;
import org.junit.Test;

import static org.junit.Assert.*;

public class ClamTest {
    @Test
    public void clamTest() {
        assertEquals(new FreshClams().toString(), "Fresh Clams from Long Island Sound");
        assertEquals(new DrunkenClams().toString(), "Drunken Clams from The Family Guy");
        assertEquals(new FrozenClams().toString(),"Frozen Clams from Chesapeake Bay");
    }
}
