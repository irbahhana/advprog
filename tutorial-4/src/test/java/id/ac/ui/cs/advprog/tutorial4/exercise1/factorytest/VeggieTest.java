package id.ac.ui.cs.advprog.tutorial4.exercise1.factorytest;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.*;

import org.junit.Test;
import static org.junit.Assert.*;

public class VeggieTest {
    @Test
    public void veggieTest() {
        assertEquals(new BlackOlives().toString(), "Black Olives");
        assertEquals(new Onion().toString(), "Onion");
        assertEquals(new Eggplant().toString(), "Eggplant");
        assertEquals(new Mushroom().toString(), "Mushrooms");
        assertEquals(new Garlic().toString(), "Garlic");
        assertEquals(new Eggplant().toString(), "Eggplant");
        assertEquals(new RedPepper().toString(), "Red Pepper");
        assertEquals(new Spinach().toString(), "Spinach");
        assertEquals(new Pineapple().toString(),"Pineapple");
    }

}
