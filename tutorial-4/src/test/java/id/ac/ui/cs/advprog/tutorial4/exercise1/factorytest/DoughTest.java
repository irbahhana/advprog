package id.ac.ui.cs.advprog.tutorial4.exercise1.factorytest;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DoughTest {
    @Test
    public void doughTest() {
        assertEquals(new StiffCrustDough().toString(), "Stiff Crust Dough");
        assertEquals(new ThickCrustDough().toString(), "Thick Crust Dough");
        assertEquals(new ThinCrustDough().toString(), "Thin Crust Dough");
    }
}
