package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NycStoreTest {
    private Pizza pizza;
    private NewYorkPizzaStore nycPizza;
    private String expected;

    @Before
    public void setUp() {
        nycPizza = new NewYorkPizzaStore();
    }

    @Test
    public void testCheesePizza() {
        pizza = nycPizza.createPizza("cheese");
        expected = "New York Style Cheese Pizza";
        assertEquals(expected, pizza.getName());
    }

    @Test
    public void testClamPizza() {
        pizza = nycPizza.createPizza("clam");
        expected = "New York Style Clam Pizza";
        assertEquals(expected, pizza.getName());

    }

    @Test
    public void testVeggiePizza() {
        pizza = nycPizza.createPizza("veggie");
        expected = "New York Style Veggie Pizza";
        assertEquals(expected, pizza.getName());
    }
}
