package id.ac.ui.cs.advprog.tutorial4.exercise1.factorytest;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SauceTest {
    @Test
    public void sauceTest(){
        assertEquals(new TartarSauce().toString(), "Tartar Sauce");
        assertEquals(new PlumTomatoSauce().toString(), "Tomato sauce with plum tomatoes");
        assertEquals(new MarinaraSauce().toString(), "Marinara Sauce");
    }
}
