package id.ac.ui.cs.advprog.tutorial4.exercise1.factorytest;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CheeseTest {

    @Test
    public void testCheese() {
        assertEquals(new FetaCheese().toString(), "Feta Cheese");
        assertEquals(new ReggianoCheese().toString(), "Reggiano Cheese");
        assertEquals(new ParmesanCheese().toString(), "Shredded Parmesan");
        assertEquals(new MozzarellaCheese().toString(), "Shredded Mozzarella");

    }

}
