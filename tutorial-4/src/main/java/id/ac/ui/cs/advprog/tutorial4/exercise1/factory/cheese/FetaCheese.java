package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class FetaCheese implements Cheese{
    @Override
    public String toString() {
        return "Feta Cheese";
    }
}
