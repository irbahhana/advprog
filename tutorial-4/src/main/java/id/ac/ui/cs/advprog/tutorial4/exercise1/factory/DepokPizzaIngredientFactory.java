package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.FetaCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.DrunkenClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.StiffCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.TartarSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.*;

public class DepokPizzaIngredientFactory implements PizzaIngredientFactory{
    @Override
    public Dough createDough() {
        return new StiffCrustDough();
    }

    @Override
    public Cheese createCheese() {
        return new FetaCheese();
    }

    @Override
    public Clams createClam() {
        return new DrunkenClams();
    }

    @Override
    public Sauce createSauce() {
        return new TartarSauce();
    }

    @Override
    public Veggies[] createVeggies() {
        return new Veggies[]{new Eggplant(),new Pineapple(), new Mushroom(), new Onion()};
    }
}
