package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class StiffCrustDough implements Dough{
    @Override
    public String toString() {
        return "Stiff Crust Dough";
    }
}
