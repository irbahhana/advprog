package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class DrunkenClams implements  Clams{
    @Override
    public String toString() {
        return "Drunken Clams from The Family Guy";
    }
}
