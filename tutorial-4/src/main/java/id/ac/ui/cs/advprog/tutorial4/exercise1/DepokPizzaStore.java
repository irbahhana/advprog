package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;

public class DepokPizzaStore extends PizzaStore {
    @Override
    protected Pizza createPizza(String type) {
        Pizza depokstyle = null;
        DepokPizzaIngredientFactory depokpizza = new DepokPizzaIngredientFactory();

        switch (type) {
            case "cheese":
                depokstyle = new CheesePizza(depokpizza);
                depokstyle.setName("Depok style cheese pizza");
                break;
            case "clam":
                depokstyle = new ClamPizza(depokpizza);
                depokstyle.setName("Depok style clam pizza");
                break;
            case "veggie":
                depokstyle = new VeggiePizza(depokpizza);
                depokstyle.setName("Depok style veggie pizza");
                break;
        }
        return depokstyle;
    }
}
