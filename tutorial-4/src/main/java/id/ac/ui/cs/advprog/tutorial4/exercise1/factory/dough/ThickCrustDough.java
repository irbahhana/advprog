package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class ThickCrustDough implements Dough {
    public String toString() {
        return "Thick Crust Dough";
    }
}
