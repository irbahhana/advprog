package hello;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CvController {
    @GetMapping("/cv")
    public String cv(@RequestParam(name= "name", required = false)
                             String name,Model model){
        model.addAttribute("name",name);
        return "cv"; // specify HTML file
    }
}
